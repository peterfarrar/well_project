const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');

const options = require('./options');

const app = express();
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: false }));
// routes:
app.use('/', require('./routes'));

// health check 
app.get('/ping', (req, res) => {
  res.status(200).send("PONG");
});

// default route
app.get('*', (req, res) => {
  console.log('In default route');
  res.status(200).send({
    message: 'default response'
  });
});

// handle unhandled errors here
app.use((err, req, res, next) => {
  // For now, just pass the error back to the front end.
  console.log('received error:', err);
  res.status(500).send({ error: err.message });
});

const port = options.port;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);

console.log('started service on port', port);
