module.exports = {
  port: parseInt(process.env.NODE_PORT, 10) || 9000,
  redisPassword: process.env.redisPassword || 'red1$Pass',
  redisHost: process.env.REDIS_HOST || '127.0.0.1',
  redisPort: process.env.REDIS_PORT || 6379,
};
