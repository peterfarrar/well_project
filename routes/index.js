const express = require('express');
const redis = require('redis');

const options = require('../options');

const app = express();
const redisClient = redis.createClient({
  host: options.redisHost,
  port: options.redisPort,
  password: options.redisPassword
});

redisClient.on('error', (error) => {
  console.log('Redis Error:', error);
  throw error;
});

// routes:
app.post('/input', (req, res) => {
  // This is the input route
  // It accepts a string, and updates or creates a Redis key/value pair
  // The initial value is 1.  Each update increments this value.
  const key = req.body;

  try {
    redisClient.get(key, (err, reply) => {
      if (err) {
        console.log('redis returned error:', err);
        throw err;
      }
      // reply has a value, but that value is not an integer
      if (reply && !Number.isInteger(reply*1)) {
        console.log('redis returned invalid value:', reply);
        throw new Error(`Invalid value for Redis key ${key}: ${reply}`);
      }

      const redisCallback = (err, reply) => {
        if (err) {
          console.log('redis returned error:', err);
          throw err;
        }
        const status = reply === "OK" ? 200 : 500;
        res.status(status).send(reply);
      };

      // start or increment count
      redisClient.set([key, (reply ? (reply*1) + 1 : 1)], redisCallback);
    });
  } catch (e) {
    throw e;
  }
});

app.get('/query', (req, res) => {
  // This is the query route
  // It accepts a 'key' parameter with a value
  // and retrieves the value of that key in Redis
  // (the value is the count of /input for that key)
  const {key} = req.query; 
  if (!key) {
    throw new Error("missing parameter 'key'");
  }

  try {
    redisClient.get(key, (err, reply) => {
      if (err) {
        console.log('redis returned error:', err);
        throw err;
      }
      // reply has a value, but that value is not an integer
      if (reply && !Number.isInteger(reply*1)) {
        console.log('redis returned invalid value:', reply);
        throw new Error(`Invalid value for Redis key ${key}: ${reply}`);
      }

      res.status(200).send(reply ? reply : '0');
    });
  } catch (e) {
    throw e;
  }
});

module.exports = app;
