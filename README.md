## This project requires NodeJS, NPM and Docker  

### The command to install and run redis in a docker container using the redis.conf file to enable password authentication:  
    docker run -p 6379:6379 --name my-redis -v /absolute/path/to/local/configuration/file/redis.conf:/usr/local/etc/redis/redis.conf -d redis /usr/local/etc/redis/redis.conf  

Notes:
- Be sure to change the `/absolute/path/to/local/configuration/file` above to the location of the repository.
- Depending on your system and user configuration, you may find you need to run the above command with `sudo`.

### To install required node modules, in the project root directory run:  
    npm i  

### To start the app, first insure the Redis instance in docker is running:  
    docker ps  

### Then run:  
    npm start  

## Testing with CURL:  
### To add or increment key values:  
    curl -w "\n" -X POST -H "Content-Type: text/plain" --data "KUH" http://localhost:9000/input  

### To retrieve the count for a key:  
    curl -w "\n" http://localhost:9000/query?key=KUH

